#!/bin/bash

# flat_no_s extra script (C) 2019 B. Roukema GPLv3 or later

# This script illustrates how to make some extra calculations for
# Roukema & Ostrowski 2019 (RO19), on the
# assumption that you have run the full script many times.  See
# arXiv:1902.09064 for details.  As of 2019, this file will be
# distributed in https://bitbucket.org/1902.09064 (to be renamed
# when an ArXiv number has been set).

printf "You will have to adjust this script for you directory names or\n"
printf "other possible changes.\n\n"

if [ $# -lt 1 ] ; then
    printf "You must give the prefix of your top directories as parameter 1 on the command line.\n"
    printf "E.g. if you have directories such as   myname*/EdSrefmodel/ , then type:\n\n"
    printf "$0 myname"
fi

## This loop adds up the detections of nonnegative curvature for
## domains reaching turnaround, for each of the reference models,
## also giving the counts of domains satisfying the turnaround
## condition (e.g. |H_D| < 1 km/s/Mpc). A 99% upper limit if there
## are zero detections is given by assuming that 5 detections should
## have occurred, but were all missed by chance; the probability of
## this is poisscdf(0,5), and 1-poisscdf(0,5) approx 0.99326.

for MODEL in EdS LCDM; do
    export BASE=$1
    grep TABLEnonneg ${BASE}*/EdSrefmodel/tmp_octave.log | \
        egrep -A3 "mbox[^ ]*${MODEL}" | \
        egrep -o " [01]/[0-9]+" | \
        awk -F / '{count +=1; nonneg+=$1; all+=$2} END {print "count=",count," nonneg=",nonneg," all=",all," 99% upp.lim.",5/all}'
done
