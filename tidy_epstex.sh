#!/bin/bash

FILE=$1

if [ -e ${FILE} ]; then
    mv -v ${FILE} ${FILE}.bak

    sed -e 's/\(unitlength.*\)\(0\.0500bp\)/\10.0200bp/' ${FILE}.bak | \
        sed -e 's,\(includegraphics\){\(/tmp/\|\),\1[scale=0.4]{,' | \
        sed -e 's,tiny,footnotesize,' \
            > ${FILE}
else
    echo "No such file:" ${FILE}
fi
