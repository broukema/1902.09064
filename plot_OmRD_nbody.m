##  plot_OmRD - script for plotting domainwise Omega curvature
##
##    Copyright (C) 2018, 2019 Boud Roukema
##
##    This program is free software; you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation; either version 2, or (at your option)
##    any later version.
##
##    This program is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with this program; if not, write to the Free Software Foundation,
##    Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
##
##    See also http://www.gnu.org/licenses/gpl.html

## global N_redshifts = 5; ## how many redshifts to plot

global L_box = {40, 160, 640};
global L_D = {2.5, 10, 40};
global hzeroeff = 0.6774

##global models = {"EdS","LCDM"};
##global models_tex = {"EdS","{\\LCDM}"};
global models = {"EdS"};
global models_tex = {"EdS"};
global times_plot = [1.0 4.0 7.0 10.0 13.0];

global varepsilonmax=[3e-7 3e-6 1e-5]


            # |HD| defines points used to estimate the turnaround OmRD
global absHDmax = 1.0
# Do not try searching for turnaround if there are less than min_lowHD low absolute H_D values
global min_lowHD = 30
global min_lowHD_posOmRD = 1

global use_aeff = 0
## tzeroeff = t0_{eff} is needed to normalise the effective scale factor
## aeff, which does not necessarily reach 1 at 13.8 Gyr, especially for EdS;
## aeff is not used at present, since the main effect is from a_D, not a_eff.
global tzeroeff = 13.8

## Tolerance for low absolute Omega_R^D values for replacing sin( ) by 3rd order Taylor
## expansion.
global tol_abs_OmRD = 1e-5

## for reproducibility, use a fixed random seed vector
global rand_seed = [3.14159e9; 2.71828e9]
rand("state",rand_seed) ## set the state of the random generator using the seed

N_times = columns(times_plot); ## how many times to plot

global do_pause1 = 0 ## for testing only
global do_pause1b = 0 ## for testing only
global do_pause2 = 0 ## for testing only
global do_pause3 = 0 ## for testing only
global do_pause4 = 0 ## for testing only

global do_quit1 = 1 ## for producing only a subset of plots

global do_Xsession = 0

## plotting boxes
global Hglob1 = 1
global Hglob2 = 1000
global OmRD1 = -10
global OmRD2 = 10
global OmmD1 = 0
global OmmD2 = 10
global OmQD1 = -5
global OmQD2 = 5
global Omm_plus_OmQ_lim = [0 -OmQD1; 0 OmQD1]

global II_OmR_axis_limits=[-0.01 0.01 OmRD1 OmRD2]
global use_RZA2_54 = 0

global OmQOmR_axis_limits=[-2 1 0 10]

global sig_from_MAD = 1.4826; ## sigma is 1.4826 * median absolute deviation for Gaussian


## should not normally be needed here - multiply H in km/s/Mpc by this to get H in Gyr^{-1}
global COSM_H_0_INV_GYR=1.02271e-3

N_box = columns(L_box)
N_models = columns(models);


## Theil-Sen linear regression - robust linear fit
## e.g. https://en.wikipedia.org/w/index.php?title=Theil%E2%80%93Sen_estimator&oldid=865140889
## - input parameters: identical to those of polyfit input parameters
function [pslope pzeropoint] = theil_sen(x_in, y_in)
  ## TODO: more checking of input
  ## size of input vectors (vertical or horizontal)
  ## TODO: error feedback for cases with two few points
  N_x = prod(size(x_in));
  N_y = prod(size(y_in));
  if (N_x != N_y)
    printf("theil_sen: Error: N_x = %d != N_y = %d\n",N_x,N_y)
    return
  endif

  if(rows(x_in) == 1 && rows(y_in) == 1) ## horizontal vectors
    x_local = repmat(x_in, N_x, 1);
    y_local = repmat(y_in, N_x, 1);
  elseif(columns(x_in) == 1 && columns(y_in) == 1) ## vertical vectors
    x_local = repmat(x_in', N_x, 1);
    y_local = repmat(y_in', N_x, 1);
  else
    printf("theil_sen: Error: the inputs must either be both row vectors or both col vectors.\n")
    return
  endif

  ## prepare to select the upper right triangle where i_col > i_row
  [i_col i_row] = meshgrid(1:N_x, 1:N_x);

  ## calculate x and y differences; and slopes
  dx = x_local' .- x_local;
  dy = y_local' .- y_local;
  slopes = (dy./dx)(i_col > i_row);
  pslope = median(slopes(isfinite(slopes))); ## ignore nans or \pm Inf from doubled x values

  ## median intercept
  pzeropoint = median(y_in .- pslope.* x_in);
endfunction

## bootstrap estimate of uncertainty in Theil-Sen zeropoint
function sig_pzeropoint = theil_sen_sig_pzeropoint(x_in, y_in)
  ## TODO: error feedback for cases with two few points
  global sig_from_MAD
  N_boot = 100;
  pz = zeros(1,N_boot);
  N_x = prod(size(x_in)); ## input sanitation is done by theil_sen, not here

  for i=1:N_boot
    j = randi(N_x, 1, N_x); ## bootstrap indices - indices allowing resampling
    [ps pz(1,i)] = theil_sen(x_in(j),y_in(j));
  endfor
  pz_median = median(pz);
  sig_pzeropoint = median(abs(pz.-pz_median)) * sig_from_MAD; ## robust estimate of variation
endfunction




function result = test_theil_sen()
  ## interactive test parameters
  show_plot = 1
  N_x_gauss = 50
  noise_amp = 1.0
  N_x_outlier = 10
  outl_noise_amp = 2.0

  N_x = N_x_gauss + N_x_outlier

  true_slope = pi
  true_zeropoint = e
  outlier_slope = -2.0
  outlier_zeropoint = 3.0

  ## gaussian scatter
  x = rand(1,N_x); # from 0 to 1
  ii_gauss = 1:N_x_gauss;
  y(ii_gauss) = (true_slope.*x(ii_gauss) .+
                 true_zeropoint .+
                 randn(1,N_x_gauss).*noise_amp);
  ## outliers - diff slope, zeropoint, noise, and add a square component
  ii_outl = N_x_gauss+1:N_x;
  y(ii_outl) = (outlier_slope.*x(ii_outl) .+
                outlier_zeropoint .-
                (x(ii_outl).^2) .+
                randn(1,N_x_outlier).*outl_noise_amp);
  if(show_plot)
    plot(x(ii_gauss),y(ii_gauss),'og;good;','markersize',5,
         x(ii_outl),y(ii_outl),'xr;bad;','markersize',5)
  endif
  hold on

  printf("\n\ntrue: %d %d\n",true_slope,true_zeropoint)
  pp = polyfit(x, y, 1);
  printf("polyfit: %d %d\n",pp(1), pp(2))
  [pslope pzeropoint] = theil_sen(x, y);
  printf("theil-sen: %d %d\n", pslope, pzeropoint)


  plot([0 1], pslope.*[0 1] .+ pzeropoint,'-k;Theil--Sen;','linewidth',5)
  plot([0 1], pp(1).*[0 1] .+ pp(2),'-b;polyfit;','linewidth',5)
  hold off
  pause
endfunction


##    lowdensity_pairs - select visually similar subset of overdense point distribution
##
## DESCRIPTION: For input arrays x_in, y_in, return a subset that
## avoids plotting visually redundant points that a human viewer will
## not be able to distinguish from one another.

## INPUTS:
## x_in, y_in = input X, Y arrays
## x1, x2 = X limits
## y1, y2 = Y limits
## N_uniform = number of points to plot if distribution is statistically
## uniform; fewer than number will almost always be selected for any
## non-grid distribution.
## N_xy = resolution in X and Y directions, copied to N_x = N_y
##
## OUTPUTS:
## x_out, y_out = output X, Y arrays that are a subset of x_in, y_in
## N_out = number of output points.

## needed for testing only
global do_Xsession = 1
global do_pause = 0

function [x_out y_out N_out] = lowdensity_pairs(x_in, y_in, x1, x2, y1, y2,
                                          N_uniform, N_xy)
  ## Resolution for density analysis; squares of size
  ## (x2-x1)*(y2-y1)/(N_x*N_y) are analysed.
  N_x = N_xy;
  N_y = N_xy;

  ## Set up internal xx, yy as row vectors and store info on  how
  ## to output the results.
  if(isrow(x_in) * isrow(y_in) * (rows(x_in) == rows(y_in)))
    inputs_are_rows = 1
    xx = x_in;
    yy = y_in;
  elseif(iscolumn(x_in) * iscolumn(y_in) * (columns(x_in) == columns(y_in)))
    inputs_are_rows = 0
    xx = x_in';
    yy = y_in';
  endif
  N_in = columns(xx)

  ## Set up ix, iy indices.
  ix = floor(N_x .* (xx .- x1)./(x2-x1));
  iy = floor(N_y .* (yy .- y1)./(y2-y1));
  ## Set up selection function: by default all points are un-selected.
  good = zeros(1,N_in);

  ## (slow) nested for loop: select subsets at high density, full sets at low density
  N_crit = N_uniform/(N_x*N_y);
  for jx=0:(N_x-1)
    for jy=0:(N_y-1)
      i_select_all = find( (jx == ix) .* (jy == iy) );
      N_found = columns(i_select_all);
      if(N_found <= N_crit)
        good(i_select_all) = 1;
      else
        good(i_select_all(randi(N_found,N_crit,1))) = 1;
      endif
    endfor
  endfor

  ## return the selection
  if(inputs_are_rows)
    x_out = xx(find(good))';
    y_out = yy(find(good))';
  else
    x_out = xx(find(good));
    y_out = yy(find(good));
  endif
  N_out = columns(x_out);
endfunction

function test_lowdensity_pairs
  global do_Xsession
  global do_pause

  N = 10000
  #N = 30
  N_uniform = 50000
  N_xy = 200

  x1 = -4.0
  x2 = 4.0
  y1 = -4.0
  y2 = 4.0

  ## initialise a big 2D data set with low density and high density regions
  x_in = randn(N,1);
  y_in = x_in .+ 0.1 .* randn(N,1);

  ## generic plot initialisation
  graphics_toolkit("gnuplot")

  if(!do_Xsession)
    ## run octave without an X session ("headless")
    set(0,'defaultfigurevisible','off')
  endif

  plot([0 1],[0 1])
  set(gcf(),"defaultlinelinewidth",10);
  set(gcf(),"defaultlinemarkersize",18);
  set(gcf(),"defaulttextfontsize",16);
  set(gcf(),"defaultaxesfontsize",18);
  set(gcf(),"defaultaxeslinewidth",5);
  set(gcf(),"defaulttextinterpreter",'tex');

  ## plot of full data
  plot(x_in,y_in,'.', 'markersize',3,'color',[0.7 0.0 0.3])
  axis([x1 x2 y1 y2])
  if(do_pause)
    pause
  endif
  eps_label = "big_eps";
  print(eps_label,'-deps')

  ## select low density subsample
  [x_out y_out N_out] = lowdensity_pairs(x_in, y_in, x1, x2, y1, y2, N_uniform, N_xy);

  ## plot the low density subsample
  plot(x_out,y_out,'.', 'markersize',3,'color',[0.7 0.0 0.3])
  axis([x1 x2 y1 y2])
  if(do_pause)
    pause
  endif
  eps_label = "subset_eps";
  print(eps_label,'-deps')
endfunction


## generic plot initialisation
graphics_toolkit("gnuplot")

if(!do_Xsession)
  ## run octave without an X session ("headless")
  set(0,'defaultfigurevisible','off')
endif

plot([0 1],[0 1])
set(gcf(),"defaultlinelinewidth",10);
set(gcf(),"defaultlinemarkersize",18);
set(gcf(),"defaulttextfontsize",16);
set(gcf(),"defaultaxesfontsize",18);
set(gcf(),"defaultaxeslinewidth",5);
set(gcf(),"defaulttextinterpreter",'tex');
##set(gcf(),"defaultaxesinterpreter",'latex');  ## obsolete as of octave 4.4.1?

## test_theil_sen()


## read ascii output files
for i_model = 1:N_models
  for i_box=1:N_box
    ## Lboxlab = sprintf("%03d",L_box{i_box});
    L_Dlab = sprintf("%05.1f",L_D{i_box});
    L_Dlab_file = regexprep( L_Dlab, '\.', 'p')
    ## "*" = date label
    filetemplate = sprintf("scalav.%s.Lbox*.L_D%s.*.nbody",
                           models{i_model},L_Dlab);
    filenames = (glob(filetemplate));
    if(length(filenames) >= 1)
      filename=filenames{1}; ## warning! this takes the first file that matches!
    else
      printf("plot_OmRD: No more files. filetemplate = %s\n",filetemplate)
      break ## out of 'for i_box=1:N_box' loop
    endif
    d = load(filename);
    N_rows_X_D = rows(d);

    if(N_rows_X_D > 1 && 1 == i_box) ## nonsense if less than 1 row, but probably should have many more
      printf("TABLEOmRD: \\mbox{%s}\n", models_tex{i_model})
      printf("TABLEOmPS_check: \\mbox{%s}\n", models_tex{i_model})
    else
      printf("plot_OmRD: Warning: %s has no rows.\n",filename)
    endif

    ## t_FLRW, H_eff, ID, a_D, H_D, OmmD, OmQD, Om_R^D = 1..8
    X_D{i_model,i_box} = d;
    ## columns
    j_t = 1
    ##j_Heff = 2
    j_ID = 6
    ##j_aD = 4
    j_HD = 2
    j_OmmD = 4
    j_OmQD = 3
    j_OmRD = 5
    #j_OmRD_ps_check = 9
    #j_II = 10 ## column added by using the *.inv file
    #j_III = 11 ## column added by using the *.inv file
    if(use_aeff)
      j_aeff = 12 ## column added by using the *.aeff file
    endif

    filename_inv = regexprep(filename, "\.dat", ".inv");
    clear d;
    d = load(filename_inv);
    N_domains = rows(d);
    invs{i_model,i_box} = d; ## I, II, III

    ## list of unique output times
    times_all = unique((X_D{i_model,i_box})(:,j_t));

    if(use_aeff)
      filename_aeff = regexprep(filename, "\.dat", ".aeff");
      clear d;
      d = load(filename_aeff);
      N_t_aeff_file = rows(d);
      if (rows(times_all) != N_t_aeff_file)
        printf("WARNING: rows(times_all) = %d != %d N_t_aeff_file\n",
               rows(times_all), N_t_aeff_file);
      endif
      aeff{i_model,i_box} = d(:,2); ## aeff(1:N_t_aeff_file)

      ## normalise aeff according to t0
      del_t_best_tmp = min(abs(d(:,1) .- tzeroeff)); # smallest diff from t_0
      i_t_min = (find(del_t_best_tmp == abs(d(:,1).-tzeroeff)))(1);
      aeff_t0 = aeff{i_model,i_box}(i_t_min);
      aeff{i_model,i_box} ./= aeff_t0;
    endif

    eps_label = sprintf("OmRD_nbody_%s_%s",
                        models{i_model},L_Dlab_file);
    printf("Analysing %s...\n",eps_label)

    for i_t=1:N_times
      ## Find the calculated time closest to the desired plottable
      ## time; if two minima exist, arbitrarily choose the lower value.
      delta_t = abs(times_all .- times_plot(i_t));
      times_use(i_t) = times_all( find( delta_t == min(delta_t) )(1) );
    endfor
    printf("times_use: ");
    times_use

    ## construct j_ID
    n_domains_input = int64(N_rows_X_D/N_times +0.01)
    domain_IDs = mod( (1:N_rows_X_D)' .- 1, n_domains_input ) .+1;
    X_D{i_model,i_box}(:,j_ID) = domain_IDs;

    ## Construct next_ID: domain IDs of the next item in any full X_D matrix.
    ## This is for finding out the final time for a given ID, assuming increasing
    ## time sequences for each ID.
    next_ID{i_model,i_box}(1:N_rows_X_D-1) = (
      X_D{i_model,i_box}(2:N_rows_X_D,j_ID) );
    ## The "next" ID of the final row is the ID of row number 1
    ## (cyclic definition of "next").
    next_ID{i_model,i_box}(N_rows_X_D) = X_D{i_model,i_box}(1,j_ID);



    ## Plot Omega_R^D for a subset of times, in order to see clear
    ## snapshots in the Omega_R^D--H_D diagram.
    for i_t=1:N_times
      i_rows_z{i_t} = find(X_D{i_model,i_box}(:,j_t) == times_use(i_t));
      frac1 = (i_t-1.0)/(max(1,N_times-1)) # blue
      frac2 = 1.0-frac1 # red

      x_in = (X_D{i_model,i_box})(i_rows_z{i_t},j_HD);
      y_in = (X_D{i_model,i_box})(i_rows_z{i_t},j_OmRD);
      N_uniform = 300000; #100000; #20000; ## result will be much less than 4096
      N_xy = 80; #40;
      [x_out y_out N_out] = (
        lowdensity_pairs(x_in, y_in,
                         Hglob1, Hglob2, OmRD1, OmRD2,
                         N_uniform, N_xy));
      printf("OmRD--H_D diagram: i_t = %d, N_out = %d\n", i_t, N_out)

      semilogx(x_out,
               y_out,
               '.','markersize',3,
               'color',[frac2, 0.0, frac1]
              )
      hold on
      legend_z{i_t} = sprintf("{\\tiny $%4.1f$~Gyr}", times_use(i_t));
      axis([Hglob1 Hglob2 OmRD1 OmRD2])
      semilogx([Hglob1],[OmRD2],
               '-; xxxxxxxxxxx  ;',
               'color',[frac2, 0.0, frac1]
              )

      printf("Minimum H_D = %f\n",
             min((X_D{i_model,i_box})(i_rows_z{i_t},j_HD)))
    endfor

    xlabel('$H_{\CD}$ (km/s/Mpc)')
    ylabel('$\OmkD$')

    set(legend,"location","northwest")
    set(legend,"fontsize",22)
    set(legend,"string",legend_z)
    legend("boxoff")

    text(Hglob1*(Hglob2/Hglob1)^0.05, -0.9*OmRD2,
         ['$\LD = ' sprintf('%5.1f',L_D{i_box}) '$~Mpc/$\hzeroeff$'],
         "fontsize",22)

    printf("Will create files %s.(eps|tex)\n",eps_label)
    print(eps_label,'-depslatex') ## for published article
    hold off

    if(do_pause1)
      pause
    endif
  endfor #i_box=1:N_box
endfor #i_model = 1:N_models

if(do_quit1)
  quit
endif

### DISCLAIMER: the rest of this file has not been tested for the N-body case ###
### Most likely it will fail unless you check it! ###

for i_model = 1:N_models
  for i_box=1:N_box

    ## plot Omega_m^D, Omega_Q^D
    ##for i_t=1:1 #:N_times
    i_lowHD = find( (abs(X_D{i_model,i_box}(:,j_HD)) < absHDmax) .*
                    (X_D{i_model,i_box}(:,j_ID) != next_ID{i_model,i_box}(:)) );

    plot((X_D{i_model,i_box})(i_lowHD,j_OmmD),
         (X_D{i_model,i_box})(i_lowHD,j_OmQD),
         '.','markersize',8
        )
    hold on
    legend_z{i_t} = sprintf("{\\tiny $%4.1f$~Gyr}", times_use(i_t));
    axis([OmmD1 OmmD2 OmQD1 OmQD2])
    ##plot([OmmD1],[OmQD2],
    ##     '-; xxxxxxxxxxx  ;',
    ##     'color',[frac2, 0.0, frac1]
    ##    )
    ##endfor

    xlabel('$\OmmD$')
    ylabel('$\OmQD$')

    set(legend,"location","northeast")
    set(legend,"fontsize",22)
    set(legend,"string",legend_z)
    ##legend("boxoff")

    text(OmmD1+(OmmD2-OmmD1)*0.5, 0.9*OmQD2,
         ['$\LD = ' sprintf('%5.1f',L_D{i_box}) '$~Mpc/$\hzeroeff$'],
         "fontsize",22)

    ## plot flatness boundary
    plot(Omm_plus_OmQ_lim(1,:),
         Omm_plus_OmQ_lim(2,:),
         '-r','linewidth',7
        )

    eps_label_Omm_OmQ = [eps_label "_Omm_OmQ"];
    printf("Will create files %s.(eps|tex)\n",eps_label_Omm_OmQ)
    print(eps_label_Omm_OmQ,'-depslatex') ## for published article
    if(do_pause1b)
      pause
    endif
    hold off


    ##
    ## copy (use plenty of memory) Invariant data to match OmRD structure
    ## j_II-th column = <II>_init
    X_D{i_model,i_box}(:,j_II) = (
      invs{i_model,i_box}( (X_D{i_model,i_box}(:,j_ID)).+1, 2 ) );

    ## j_III-th column = <III>_init
    X_D{i_model,i_box}(:,j_III) = (
      invs{i_model,i_box}( (X_D{i_model,i_box}(:,j_ID)).+1, 3 ) );

    if(use_aeff)
      ## j_aeff-th column = aeff
      ## for each i_ID, match up times up to turnaround
      for i_ID = min(X_D{i_model,i_box}(:,j_ID)):max(X_D{i_model,i_box}(:,j_ID))
        ## find rows in X_D for this particular i_ID
        i_rows_1ID = find( i_ID == X_D{i_model,i_box}(:,j_ID) );
        n_rows_1ID = rows(i_rows_1ID);

        ## add times in column j_aeff for those rows in X_D
        if(n_rows_1ID > 0)
          X_D{i_model,i_box}(i_rows_1ID,j_aeff) = (
            aeff{i_model,i_box}( 1:n_rows_1ID )
          );
        endif
      endfor
    endif #     if(use_aeff)

    ## Select domains at arbitrary times where |HD| < absHDmax
    ## at the final pre-turnaround time.
    i_lowHD = find(  (abs(X_D{i_model,i_box}(:,j_HD)) < absHDmax) .*
                     (X_D{i_model,i_box}(:,j_ID) != next_ID{i_model,i_box}(:)) );

    alpha2 = 1.0;
    plot(X_D{i_model,i_box}(i_lowHD,j_II), X_D{i_model,i_box}(i_lowHD,j_OmRD) ./alpha2,
         '.','markersize',8) #3)

    ## calculate and plot a robust linear fit only if there are enough points
    if(rows(i_lowHD) >= min_lowHD)
      ## i_lowII = find(abs(X_D{i_model,i_box}(:,j_II)) < IImax);
      ##i_lowHD_lowII = intersect(i_lowHD, i_lowII);
      ## plot OmRD as fn of <II>_init
      [pslope pzero] = theil_sen(X_D{i_model,i_box}(i_lowHD,j_II),
                                 X_D{i_model,i_box}(i_lowHD,j_OmRD) ./alpha2)
      pp = [pslope pzero];
      sig_pzeropoint = theil_sen_sig_pzeropoint(X_D{i_model,i_box}(i_lowHD,j_II),
                                 X_D{i_model,i_box}(i_lowHD,j_OmRD) ./alpha2)
      printf("TABLEOmRD: & %9.2f \\pm %9.2f\n",pzero, sig_pzeropoint)

      ## Do the same thing, but instead of OmRD, print out
      ## Omega_RD_planesym_check, which should be -6 in the
      ## plane-symmetric subcase.

      [qslope qzero] = theil_sen(X_D{i_model,i_box}(i_lowHD,j_II),
                                 X_D{i_model,i_box}(i_lowHD,j_OmRD_ps_check))
      qq = [qslope qzero];
      sig_qzeropoint = theil_sen_sig_pzeropoint(X_D{i_model,i_box}(i_lowHD,j_II),
                                 X_D{i_model,i_box}(i_lowHD,j_OmRD_ps_check))
      printf("TABLEOmPS_check: & %9.2f \\pm %9.2f\n",qzero, sig_qzeropoint)

      ## statistics of OmRD for low HD
      stats_OmRD{i_model,i_box} = statistics( (X_D{i_model,i_box}(i_lowHD,j_OmRD) ./alpha2)' );
      ## interquartile range of III
      stats_OmRD{i_model,i_box}(10) = (
        sig_from_MAD*
        median(abs((X_D{i_model,i_box}(i_lowHD,j_OmRD) ./alpha2)' .- (stats_OmRD{i_model,i_box})(3)))
      );
      k_OmRD_stats=[6 7 3 10];
      label_OmRD_stats={"\\mu", "\\sigma", "\\mu'","\\sigma'"};


      stats_III = statistics( (X_D{i_model,i_box}(:,j_III))' )
      ## interquartile range of III
      III_IQR = stats_III(4)-stats_III(2)
      frac_IQR = 0.25

      i_lowHD_lowIII = find( (abs(X_D{i_model,i_box}(:,j_HD)) < absHDmax) .*
                             (X_D{i_model,i_box}(:,j_ID) != next_ID{i_model,i_box}(:)) .*
                             (abs(X_D{i_model,i_box}(:,j_III)) < frac_IQR*III_IQR) );

      if(rows(i_lowHD_lowIII) >= min_lowHD)
        [pslopeIII pzeroIII] = theil_sen(X_D{i_model,i_box}(i_lowHD_lowIII,j_II),
                                         X_D{i_model,i_box}(i_lowHD_lowIII,j_OmRD))
        ppIII = [pslopeIII pzeroIII];
        sig_pzeropointIII = theil_sen_sig_pzeropoint(X_D{i_model,i_box}(i_lowHD_lowIII,j_II),
                                                      X_D{i_model,i_box}(i_lowHD_lowIII,j_OmRD))

        printf("TABLE_III_OmRD: & %9.2f \\pm %9.2f\n",pzeroIII, sig_pzeropointIII)

      endif

      hold on
      IImin = min(X_D{i_model,i_box}(i_lowHD,j_II));
      IImax = max(X_D{i_model,i_box}(i_lowHD,j_II));
      ##plot([IImin IImax],polyval(pp,[IImin IImax]),'-r;Theil--Sen linear;','linewidth',5)
      plot([IImin IImax],polyval(pp,[IImin IImax]),'-k','linewidth',7)
    endif #  if(rows(i_lowHD) >= min_lowHD)

    IImin = min(X_D{i_model,i_box}(i_lowHD,j_II));
    IImax = max(X_D{i_model,i_box}(i_lowHD,j_II));
    ## plot flatness boundary
    plot([IImin IImax],[0 0],'-r','linewidth',7)
    if(do_pause2)
      pause
    endif

    xlabel('$\initinvII$')
    ylabel(['$\OmRD\vert_{|H_{\CD}|<' sprintf("%2.0f",absHDmax) '\mathrm{~km/s/Mpc}}$'])
    axis(II_OmR_axis_limits)

    ##legend_II_OmRD = {"Theil--Sen linear"};
    ##set(legend,"location","northwest")
    ##set(legend,"fontsize",22)
    ##set(legend,"string",legend_II_OmRD)
    ##legend("boxoff")
    eps_label_II = [eps_label "_II_OmRD"];

    xl = II_OmR_axis_limits(1:2);
    yl = II_OmR_axis_limits(3:4);
    text(xl(1) + (xl(2)-xl(1))*0.05, yl(1)+0.9*(yl(2)-yl(1)),
         ['$\LD = ' sprintf('%5.1f',L_D{i_box}) '$~Mpc/$\hzeroeff$'],
         "fontsize",22)

    printf("Will create files %s.(eps|tex)\n",eps_label_II)
    print(eps_label_II,'-depslatex') ## for published article

    hold off

    if(rows(i_lowHD) >= min_lowHD)
      found_turnaround{i_model,i_box} = 1
    else
      printf("TABLEOmRD: & \\mbox{--}\n")
      printf("TABLEOmPS_check: & \\mbox{--}\n")
      found_turnaround{i_model,i_box} = 0
    endif #  if(rows(i_lowHD) >= min_lowHD)



    ## plot epsilon a dimensionless domain-scale curvature-induced deviation parameter
    ## 1/6 (c L_D/H_eff)^2 /OmRD

    for i_t=1:N_times
      ## The following should be identical, i.e. zero sigma
      s = (statistics(((X_D{i_model,i_box})(i_rows_z{i_t},j_Heff))'))(1:7)
      H_eff = s(3) ## could just as well be the mean rather than the median
      eps_taylor3 = -(((1.0/6.0) *
                      (L_D{i_box}*hzeroeff * H_eff /299792.458)^2)
                     .* (X_D{i_model,i_box}(i_rows_z{i_t},j_aD)).^2
                     .* (X_D{i_model,i_box})(i_rows_z{i_t},j_OmRD));


      ## Exact sin or sinh formula for non-zero OmegaRD.
      ## The curvature radius LReff is real for positive curvature (neg OmRD);
      ## and imaginary for negative curvature (pos OmRD); octave should be
      ## able to handle the cancellation of 'i' when it calculates 'eps'.
      LReff = ( 1.0 ./
                ( X_D{i_model,i_box}(i_rows_z{i_t},j_aD) .*
                  (H_eff /299792.458) .*
                  sqrt( -(X_D{i_model,i_box})(i_rows_z{i_t},j_OmRD) )
                )
              );
      eps = (( LReff .* sin( (L_D{i_box}*hzeroeff) ./ LReff ) .- L_D{i_box}*hzeroeff)
             ./ (L_D{i_box}*hzeroeff)
            );

      ## If close to flat, then override the sin|sinh calculation
      ## using 3rd order Taylor.
      i_low_OmRD = find( abs( X_D{i_model,i_box}(i_rows_z{i_t},j_OmRD) ) < tol_abs_OmRD );
      rows(i_low_OmRD)
      columns(i_low_OmRD)
      if(rows(i_low_OmRD) >= 1)
        eps(i_low_OmRD) = eps_taylor3(i_low_OmRD);
      endif

      x_in = (X_D{i_model,i_box})(i_rows_z{i_t},j_HD);
      y_in = eps;
      N_uniform = 300000; #100000; #20000; ## result will be much less than 4096
      N_xy = 80; #40;
      [x_out y_out N_out] = (
        lowdensity_pairs(x_in, y_in,
                         Hglob1, Hglob2, -varepsilonmax(i_box), varepsilonmax(i_box),
                         N_uniform, N_xy));

      frac1 = (i_t-1.0)/(max(1,N_times-1)) # blue
      frac2 = 1.0-frac1 # red
      semilogx(x_out,
               y_out,
               '.','markersize',3,
               'color',[frac2, 0.0, frac1]
              )
      hold on
      axis([Hglob1 Hglob2 -varepsilonmax(i_box) varepsilonmax(i_box)])
      semilogx([Hglob1],[0.0],
               '-; xxxxxxxxxxx  ;',
               'color',[frac2, 0.0, frac1]
              )

      if(do_pause3)
        pause
      endif
    endfor

    xlabel('$H_{\CD}$ (km/s/Mpc)')
    ylabel('$\varepsilon$')

    set(legend,"location","northwest")
    set(legend,"fontsize",22)
    set(legend,"string",legend_z)
    legend("boxoff")
    ## eps = extended postscript; epsil = epsilon :)
    eps_label_epsil = [eps_label "_epsilon"];

    text(Hglob1*(Hglob2/Hglob1)^0.05, -0.9*varepsilonmax(i_box),
         ['$\LD = ' sprintf('%5.1f',L_D{i_box}) '$~Mpc/$\hzeroeff$'],
         "fontsize",22)

    printf("Will create files %s.(eps|tex)\n",eps_label_epsil)
    print(eps_label_epsil,'-depslatex') ## for published article
    if(do_pause3)
      pause
    endif
    hold off

    if(N_box == i_box)
      printf("TABLEOmRD: \\\\\n")
      printf("TABLEOmPS_check: \\\\\n")
    endif
  endfor

  ## What enables non-negative OmRD turnaround? How frequent is it?
  symlist = ['o','x','+'];
  collist = {'blue',[0 0.7 0],'red'};
  i_plottable = 0 ## number of cases that can be plotted
  clear legend_LD


  if(i_model <= rows(X_D)) ## if only EdS is done, then ignore LCDM
    printf("TABLEnonnegOmRD: \\mbox{%s}\n", models_tex{i_model})

    for i_box=1:N_box

      ## Select domains at arbitrary times where:
      ## |HD| < absHDmax AND
      ## OmRD is non-negative
      i_lowHD_non_unique = (
        find( (abs(X_D{i_model,i_box}(:,j_HD)) < absHDmax) ));
      N_lowHD_non_unique = rows(i_lowHD_non_unique);

      ## Select domains at arbitrary times where:
      ## |HD| < absHDmax AND
      ## the final time for a given domain is considered AND
      ## OmRD is non-negative
      i_lowHD = (
        find( (abs(X_D{i_model,i_box}(:,j_HD)) < absHDmax)  .*
              (X_D{i_model,i_box}(:,j_ID) != next_ID{i_model,i_box}(:)) )
      );
      N_lowHD = rows(i_lowHD);

      i_lowHD_posOmRD = (
        find( (abs(X_D{i_model,i_box}(:,j_HD)) < absHDmax) .*
              (X_D{i_model,i_box}(:,j_ID) != next_ID{i_model,i_box}(:)) .*
              (X_D{i_model,i_box}(:,j_OmRD) > 0.0)
            ));
      N_lowHD_posOmRD = rows(i_lowHD_posOmRD);
      if(N_lowHD > 0)
        if(use_RZA2_54)
          printf("TABLEnonnegOmRD: & %9.3f \n",
                 N_lowHD_posOmRD/N_lowHD)
        else
          printf("TABLEnonnegOmRD: & %d/%d \n",
                 N_lowHD_posOmRD,
                 N_lowHD
                )
        endif
      else
        printf("TABLEnonnegOmRD: & \\mbox{--} \n")
      endif

      if(rows(i_lowHD_posOmRD) >= min_lowHD_posOmRD)
        if(!use_RZA2_54)
          for ii_row = i_lowHD_posOmRD'
            ## check if this is really the first-time turnaround epoch
            ## find the domain ID number
            ii_ID = X_D{i_model,i_box}(ii_row,j_ID);
            t_ii = X_D{i_model,i_box}(ii_row,j_t);
            ## find the global row numbers with the same ID and earlier times
            i_earlier = find( (ii_ID == X_D{i_model,i_box}(:,j_ID)) .*
                              (X_D{i_model,i_box}(:,j_t) < t_ii) );
            min_HD_pre_turnaround = min( X_D{i_model,i_box}(i_earlier,j_HD) )
            ## Set a logical value 'near_TA':
            ## 1 if no earlier strongly negative HD occurred - a correct detection
            ## 0 otherwise - a false detection (e.g. average volume oscillating)
            near_TA = (min_HD_pre_turnaround > -absHDmax)

            printf("VALUESnonnegOmRD: %d & %s & %9.5f & %9.5f   & %9.5f & %9.5f   & %9.5f & %9.5f & %9.5f \n",
                   near_TA,
                   models_tex{i_model},
                   L_D{i_box},
                   X_D{i_model,i_box}(ii_row,j_t),
                   X_D{i_model,i_box}(ii_row,j_HD),
                   min_HD_pre_turnaround,
                   X_D{i_model,i_box}(ii_row,j_II),
                   X_D{i_model,i_box}(ii_row,j_OmQD),
                   X_D{i_model,i_box}(ii_row,j_OmRD))
            printf("VALUESnonnegOmRD: \\\\\n")
          endfor
        endif
        plot(X_D{i_model,i_box}(i_lowHD_posOmRD,j_OmQD),
             X_D{i_model,i_box}(i_lowHD_posOmRD,j_OmRD),
             [symlist(i_box) '; xxxxxxxxxxx ;'],
             'markersize',8,
             'color',collist{i_box})

        i_plottable++ ;
        legend_LD{i_plottable} = (
          ['$\LD = ' sprintf('%5.1f',L_D{i_box}) '$~Mpc/$\hzeroeff$']);

        hold on
        if(do_pause4)
          pause
        endif
      endif
    endfor # for i_box=N_box:-1:1

    printf("TABLEnonnegOmRD: \\\\\n")

    if(i_plottable > 0)
      xlabel('$\OmQD$')
      ylabel(['$\OmRD\vert_{|H_{\CD}|<' sprintf("%2.0f",absHDmax) '\mathrm{~km/s/Mpc}}$'])

      set(legend,"location","northeast")
      set(legend,"fontsize",22)
      set(legend,"string",legend_LD)
      legend("boxoff")
      legend("left")

      eps_label = sprintf("OmRD_scalav_%s",
                          models{i_model});
      eps_label_OmQOmR = [eps_label "_OmQOmR"];

      axis(OmQOmR_axis_limits);

      xl = xlim;
      yl = ylim;
      text(xl(1) + (xl(2)-xl(1))*0.13, yl(1)+0.9*(yl(2)-yl(1)),
           models_tex{i_model},
           "fontsize",22,
           "horizontalalignment","right"
          )

      printf("Will create files %s.(eps|tex)\n",eps_label_OmQOmR)
      print(eps_label_OmQOmR,'-depslatex') ## for published article

      hold off
    endif # if(i_plottable > 0)

    if(do_pause4)
      pause
    endif
  endif #if(i_model <= rows(OmRD)) ## if only EdS is done, then ignore LCDM

endfor


## print OmRD general statistics
for i_model=1:N_models
  printf("TABLE_stats_OmRD: & \\multicolumn{3}{c}{\\mbox{%s}} \\\\\n",models_tex{i_model})

  for i_stats_type=1:4
    printf("TABLE_stats_OmRD: %s(\\OmRD) ",label_OmRD_stats{i_stats_type})
    for i_box=1:N_box
      if(found_turnaround{i_model,i_box})
        printf("& %9.2f ",(stats_OmRD{i_model,i_box})(k_OmRD_stats(i_stats_type)))
      else
        printf("& \\mbox{--} ")
      endif
    endfor
    if(1==i_stats_type)
      printf("\\\\\n")
    else
      printf("\\rule{0ex}{2.5ex} \\\\\n")
    endif
  endfor # for i_stats_type=1:4

endfor ## for i_model=1:N_models
