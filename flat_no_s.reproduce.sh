#!/bin/bash

# flat_no_s script (C) 2018-2019, 2023 B. Roukema GPLv3 or later

# This script should install mpgrafic, DTFE, inhomog, RAMSES and
# ramses-scalav, run the calculations, and make the plots required for
# Roukema & Ostrowski 2019 (RO19).  See
# arXiv:1902.09064 for details.  As of 2019, this file will be
# distributed in https://bitbucket.org/broukema/1902.09064 (to be renamed
# when an ArXiv number has been set).

echo "This script is derived from INSTALL.ramses-scalav.shell.experts.sh ."
echo "It may well fail on your system. Do not say that you have not been warned.\n"
sleep 1

printf "\nOnce you are ready to run this, follow this Usage:\n\n"
printf "$0 DIRECTORY N YES\n\n"
printf "where DIRECTORY is the directory where you want to install\n"
printf "everything), and N is 32, 64, 128, or 256 for\n"
printf "using N^3 particles for the initial conditions,\n"
printf "and YES is literally the string YES.\n\n"
printf "The recommended default for N is 64.\n\n"
printf "Higher values of N will give fuller results and better match the paper.\n"
printf "\nHowever, you should first read through the script and check\n"
printf "whether it can reasonably be expected to do what you would like it\n"
printf "to do.\n\n"
printf "To create both the EdS and LCDM plots and table entries, edit the\n"
printf "script and set EDS_ONLY=NO. This is much slower than EdS only.\n\n"

printf "This script is intended to help in reproducibility of published\n"
printf "results. However, the free (as in speech) software ecosystem keeps\n"
printf "evolving, with bug fixes, addition of features, obsoletion of old\n"
printf "functions and interfaces, and attempts to improve sustainability.\n"
printf "Your constructive bug reports will help improve software for the\n"
printf "benefit of the whole community:\n"
printf "  https://www.chiark.greenend.org.uk/~sgtatham/bugs.html .\n"

MPGRAFIC_COMMIT_HASH=$(cat commit.mpgrafic)
DTFE_COMMIT_HASH=$(cat commit.dtfe)
#The following is for not-yet-public development branches - most users can safely ignore it:
GIT_INHOMOG_DEV=${GIT_INHOMOG_DEV}
INHOMOG_COMMIT_HASH=$(cat commit.inhomog)
RAMSES_COMMIT_HASH=a317f07
RAMSES_COMMIT_BRANCH2=allow_64bit_int_postrum2017
RAMSES_COMMIT_HASH2=$(cat commit.ramses)
#The following is for not-yet-public development branches - most users can safely ignore it:
GIT_RAMSES_SCALAV_DEV=${GIT_RAMSES_SCALAV_DEV}
RAMSES_SCALAV_COMMIT_HASH=$(cat commit.ramses-scalav)
RAMSES_SCALAV_NBODY_COMMIT_HASH=$(cat commit.ramses-scalav-nbody)


## Choose the numbers of CPUs for mpgrafic (openmp) and ramses (mpi);
## the numbers for the dtfe and inhomog libraries (openmp) are not (presently) constrained.
## Parallel compilation (in cases where this is known to work) uses N_CPUS_DEFAULT.
N_CPUS_DEFAULT=2
N_CPUS_MPGRAFIC=${N_CPUS_DEFAULT}
N_CPUS_RAMSES=${N_CPUS_DEFAULT}

## You should not *have* to use autoreconf, but generally this might help
## to create an up-to-date "configure" file for the ./configure command.
#DO_AUTORECONF=YES
DO_AUTORECONF=NO

## The script will run faster without checks, but you are less likely to detect
## portability/software evolution problems or other bugs early if you turn this off.
DO_CHECKS=YES
#DO_CHECKS=NO

## Try an existing set of packages installed in the same top directory?
TRY_OLD=YES
#TRY_OLD=NO

## Set HAVE_INTERNET=NO to disable git cloning|pulling; otherwise internet access will be assumed.
#HAVE_INTERNET=NO
HAVE_INTERNET=YES
#HAVE_INTERNET=anything_else


## Running the LCDM calculations used to be an order of magnitude
## slower than running the EdS calculations, but is now only
## a little slower. If you wish to reproduce only the EdS plots
## then set EDS_ONLY to YES.
EDS_ONLY=NO
#EDS_ONLY=YES

#default
RUN_NBODY=NO
#override
if [ "x$4" = "xNBODY" ]; then
    ## Running full N-body simulations is much heavier than RZA
    ## simulations; this is necessary for the extra figure in version
    ## 2 (v2) of the article.  This parameter can be turned on by
    ## adding a fourth command line parameter as the string NBODY; this
    ## disables the default RZA calculations.
    RUN_NBODY=YES
fi

## List of raw box sizes in Mpc/h, that are adjusted depending on the
## cube root of the number of particles N_CROOT. For N_CROOT==256,
## this is the literal (FLRW reference model comoving) fundamental domain size.
LBOX_FACT16_LIST="640 160 040"

if [ "x${RUN_NBODY}" = "xYES" ]; then
    EDS_ONLY=YES
fi

######################################################################
## You are unlikely to have to modify anything below this line. ######
######################################################################

## list of reproducibility files apart from this file itself
REPRODUCE_FILES="README plot_OmRD.m tidy_epstex.sh show_figures_all.tex show_figures_EdS_only.tex"
if [ "x${RUN_NBODY}" = "xYES" ]; then
    REPRODUCE_FILES+=" plot_OmRD_nbody.m show_figures_nbody.tex"
fi
REPRODUCE_FILES+=" commit.mpgrafic commit.dtfe commit.inhomog commit.ramses commit.ramses-scalav"
REPRODUCE_FILES+=" commit.ramses-scalav-nbody"


## three parameters for codeberg repository: DEVELOPER PACKAGE HASH
function clone_codeberg {
    printf "Will try to git clone or update $1/$2 at commit $3...\n"
    ## Do this provided that the internet is not cut off.
    if ! [ "x${HAVE_INTERNET}" = "xNO" ] ; then
        if [ "x${TRY_OLD}" = "xYES" ]; then
            if ( cd $2 && (git pull ; cd ..) ); then
                echo "git pull in $2 OK"
            else
                git clone https://codeberg.org/$1/$2
            fi
        else
            git clone https://codeberg.org/$1/$2
        fi
    else
        printf "Warning: HAVE_INTERNET=NO - will not try git cloning|pulling.\n"
    fi
    cd $2
    WE_HAVE=$(git log --oneline |head -n 1 | awk '{print $1}')
    printf "Package $2: we have: ${WE_HAVE} .\n"
    if [ "x${WE_HAVE}" = "x$3" ]; then
        echo "The git clone and commit version seem to be correct."
    else
        # Try to checkout the required version and test again.
        git checkout $3
        WE_HAVE=$(git log --oneline |head -n 1 | awk '{print $1}')
        printf "Package $2: we have now shifted to: ${WE_HAVE} .\n"
        if [ "x${WE_HAVE}" = "x$3" ]; then
            echo "The git clone and checked out commit seem to be correct."
        else
            echo "The git clone or commit version seem to have a problem."
            cd .. # return to root install directory
            exit 1
        fi
    fi
    cd .. # return to root install directory
}


## three parameters for bitbucket repository: DEVELOPER PACKAGE HASH
function clone_bitbucket {
    printf "Will try to git clone or update $1/$2 at commit $3...\n"
    ## Do this provided that the internet is not cut off.
    if ! [ "x${HAVE_INTERNET}" = "xNO" ] ; then
        if [ "x${TRY_OLD}" = "xYES" ]; then
            if ( cd $2 && (git pull ; cd ..) ); then
                echo "git pull in $2 OK"
            else
                git clone https://bitbucket.org/$1/$2
            fi
        else
            git clone https://bitbucket.org/$1/$2
        fi
    else
        printf "Warning: HAVE_INTERNET=NO - will not try git cloning|pulling.\n"
    fi
    cd $2
    WE_HAVE=$(git log --oneline |head -n 1 | awk '{print $1}')
    printf "Package $2: we have: ${WE_HAVE} .\n"
    if [ "x${WE_HAVE}" = "x$3" ]; then
        echo "The git clone and commit version seem to be correct."
    else
        # Try to checkout the required version and test again.
        git checkout $3
        WE_HAVE=$(git log --oneline |head -n 1 | awk '{print $1}')
        printf "Package $2: we have now shifted to: ${WE_HAVE} .\n"
        if [ "x${WE_HAVE}" = "x$3" ]; then
            echo "The git clone and checked out commit seem to be correct."
        else
            echo "The git clone or commit version seem to have a problem."
            cd .. # return to root install directory
            exit 1
        fi
    fi
    cd .. # return to root install directory
}


function do_the_plots {
    ## generate the plots as .eps with text in matching .tex files for
    ## font consistency, and the table(s) in .tex format

    cd EdSrefmodel

    if [ "x${RUN_NBODY}" = "xYES" ]; then
            case ${N_CROOT} in
                32)
                    sed -e 's/\(L_box.*\) *40, *160, *640/\120, 80, 320/' ../plot_OmRD_nbody.m > plot_OmRD_nbody_lboxfix.m
                    ;;
                *)
                    ln -s ../plot_OmRD_nbody.m plot_OmRD_nbody_lboxfix.m
                    ;;
            esac
        octave --no-gui plot_OmRD_nbody_lboxfix.m 2>&1 | tee tmp_octave.log
    else
        octave --no-gui ../plot_OmRD.m 2>&1 | tee tmp_octave.log | \
            grep TABLEOmRD | sed -e 's/^TABLEOmRD://' > OmRD_TA_table.tex

        grep TABLEOmPS_check tmp_octave.log | \
            sed -e 's/^TABLEOmPS_check://' > OmRD_TA_table_planesym.tex

        grep TABLEnonnegOmRD tmp_octave.log | \
            sed -e 's/^TABLEnonnegOmRD://' > nonnegOmRD_TA_table.tex
        ## The following is mainly for checking manually. If true
        ## detections (first column = 1) are found it could be useful as a
        ## table, otherwise use it just in text discussion.
        grep VALUESnonnegOmRD tmp_octave.log | \
            sed -e 's/^VALUESnonnegOmRD://' > nonnegOmRD_TA_values.tex

        grep TABLE_stats_OmRD tmp_octave.log | \
            sed -e 's/^TABLE_stats_OmRD://' > statsOmRD_TA_values.tex
    fi

    printf "\n\n=======tmp_octave.log:\n"
    cat tmp_octave.log
    printf "\n\n/tmp_octave.log=======\n"

    ## tidy the .tex files and compile the figures into an example pdf file
    for i in $(ls OmRD_*_*tex); do ../tidy_epstex.sh $i ; done
    pdflatex ../show_figures
    cp -pv show_figures.pdf ../
}

function report_system_diagnostics {
    printf "\n****diagnostics:\n"
    md5sum $0 # What file am I? Are we talking about the same script?
    date
    uname -a
    free -h
    printf "\n****diagnostics:\n"
    cat /etc/debian_version
    printf "\n****diagnostics:\n"
    gcc --version
    printf "\n****diagnostics:\n"
    g++ --version
    printf "\n****diagnostics:\n"
    gfortran --version
    printf "\n****diagnostics:\n"
    mpifort --version
    printf "\n****diagnostics:\n"
    ls -lt ${MYUSR}/lib/* ${MYUSR}/include/* ${MYUSR}/bin/*
    printf "\n****diagnostics:\n"
    echo "PATH=${PATH}"
    echo "MANPATH=${MANPATH}"
    echo "LDFLAGS=${LDFLAGS}"
    echo "CFLAGS=${CFLAGS}"
    echo "FFLAGS=${FFLAGS}"
    echo "LD_LIBRARY_PATH=${LD_LIBRARY_PATH}"
    printf "\n****diagnostics:\n"
    which mpgrafic_params.sh
    which mpgrafic
    which inhomog
    which ramses3d
    printf "\n****diagnostics:\n"
    mpgrafic --version
    inhomog --version
    ramses3d --version
    printf "\n****diagnostics:\n"
    for binary in $(ls ${MYUSR}/bin/*|grep -v "\.sh"); do
        echo "ldd ${binary}:\n"
        ldd ${binary}
    done

    date
    printf "..../diagnostics****\n"
}


function check_binary_path {
    BINARY=$1
    SHOULD_BE_PATH=$2

    WHICH_BINARY=$(which ${BINARY})

    if [ "x${WHICH_BINARY}" != "x${SHOULD_BE_PATH}" ]; then
        printf "WARNING: ${BINARY} is supposed to be a freshly compiled user level program.\n"
        printf "However, you have ${BINARY} installed as ${WHICH_BINARY},\n"
        printf "and your PATH environment variable\n"
        printf "{PATH}\n"
        printf "prefers to use ${WHICH_BINARY} instead of \n"
        printf "${SHOULD_BE_PATH}.\n"
        printf "Maybe you should temporarily rename ${WHICH_BINARY}, for example:\n\n"
        printf "'mv -iv ${WHICH_BINARY} ${WHICH_BINARY}_temporary'\n"
    fi
}


if [ $# -eq 0 ]; then
    exit 0
fi

FULL_INSTALL_DIR=$1

if [ ! -d "${FULL_INSTALL_DIR}" ]; then
    printf "Cannot find directory '${FULL_INSTALL_DIR}'.\n"
    exit 1
fi
for file in ${REPRODUCE_FILES}; do
    if !(cp -pv --dereference ${file} ${FULL_INSTALL_DIR}); then
        ## PWD might be the same as FULL_INSTALL_DIR
        if ! [ -r ${FULL_INSTALL_DIR}/${file} ]; then
            printf "You must copy the file '${file}' and all other files from\n"
            printf "the reproduce/ directory into this present directory ${PWD}\n"
            printf "so that this script can copy it into ${FULL_INSTALL_DIR}.\n"
            printf ""
            exit 1
        fi
    fi
done
OLD_DIR=${PWD}
cd ${FULL_INSTALL_DIR}

## Both models, or only EdS?
if [ "x${EDS_ONLY}" = "xYES" ] ;then
    FLRW_LIST="EdS"
    FLRW_TARGET=show_figures_EdS_only.tex
else
    FLRW_LIST="EdS LCDM"
    FLRW_TARGET=show_figures_all.tex
fi

if [ "x${RUN_NBODY}" = "xYES" ]; then
    # override the above options
    FLRW_LIST="EdS"
    FLRW_TARGET=show_figures_nbody.tex
fi

if !(ln -s ${FLRW_TARGET} show_figures.tex); then
        rm show_figures.tex;
        if !(ln -s ${FLRW_TARGET} show_figures.tex); then
            printf "Please fix the error that blocks creating a symbolic link 'show_figures.tex'\n"
            printf "to the normal file '${FLRW}'. Maybe the former already exists or\n"
            printf "the latter does not?\n"
            exit 1
        fi
fi


export MYUSR=$(/bin/pwd)
printf "\nI will install everything in '${MYUSR}'.\n"

if [ $# -lt 3 ]; then
    printf "But first you must retype the command with YES as the second argument.\n"
    exit 0
fi

N_CROOT=$2
if ! ( [ "x$2" = "x32" ] || [ "x$2" = "x64" ] || [ "x$2" = "x128" ] || [ "x$2" = "x256" ] ); then
    printf "Invalid resolution N = $2. Please choose 32|64|128|256.\n"
    exit 1
fi

REALLY=$3

if [ "x${REALLY}" = "xYES" ] ; then


    # 0. ENVIRONMENT
    #
    #    Set up or choose a user space directory tree for libraries,
    #    include files, binaries (and in some cases, man pages). This
    #    should be throwaway space that is not backed up, since all
    #    these files can be trivially re-created.
    #    Define an environment variable for this, e.g.

    ## 0.1 installation directories and environment variables
    mkdir -p ${MYUSR} ${MYUSR}/lib ${MYUSR}/include ${MYUSR}/bin

    #   You will then need to update environment variables based on
    #    this main directory:

    export PATH=${MYUSR}/bin:${PATH}
    export MANPATH=${MYUSR}/share/man:${MANPATH}
    ## prefer LDFLAGS provided here rather than default ones
    OLD_LDFLAGS=${LDFLAGS}
    export LDFLAGS="-L${MYUSR}/lib ${LDFLAGS}"
    export FFLAGS="${FFLAGS} -I${MYUSR}/include"

    ## 0.2 Install system-level libraries with 'sudo', e.g. on
    ## Debian/stretch or Ubuntu, if needed. For stable well-tested
    ## packages, this is generally safer and easier than installing
    ## packages by hand. When not connected to the internet, these
    ## packages can be installed from a rescue disk or any other local
    ## archive of debian (or derivative) packages.

    ## list of dependencies for each component:
    BASICS_DEP="gcc;g++;gfortran"
    MPGRAFIC_DEP="libopenmpi-dev;openmpi-bin;fftw-dev;libgsl-dev"
    DTFE_DEP="libcgal-dev;libboost-filesystem-dev"
    PLOT_RESULTS_DEP="octave;texlive-latex-base;texlive-latex-extra;fig2dev;epstool"


    if (which dpkg); then
        printf "You appear to have the following packages already installed in\n"
        printf "your system:\n\n"
        dpkg -l | egrep "mpgrafic|inhomog|ramses3d"
        which mpgrafic inhomog ramses3d
        printf "\nthough this should not be a problem. This is only a warning.\n\n"
    fi

    ## Do this provided that the internet is not cut off.
    if ! [ "x${HAVE_INTERNET}" = "xNO" ] ; then
        printf "If you wish to install system-level packages independently\n"
        printf "of this script, then try:\n\n"
        printf "\nsudo aptitude update && "
        printf "sudo aptitude install "
        echo ${BASICS_DEP} ${MPGRAFIC_DEP} ${DTFE_DEP} ${PLOT_RESULTS_DEP}|sed -e 's/;/ /g'
        printf "\n\n"
        printf "and then re-run the script (which will be happy that the packages are installed).\n"
        printf "On the Knoppix live DVD, you might wish to instead do:\n\n"
        printf "sudo apt-get update && "
        printf "sudo apt-get -t testing --allow-downgrades --assume-yes install "
        echo ${BASICS_DEP} ${MPGRAFIC_DEP} ${DTFE_DEP} ${PLOT_RESULTS_DEP}|sed -e 's/;/ /g'
        printf "\n\n"
        printf "***If you do not 'control C' now,*** then you will most likely be asked\n"
        printf "for your user password so that the above system-level packages and their\n"
        printf "dependencies can be installed using 'sudo' - as root.\n\n"
        sleep 10

        for NEED_LIBS in "${BASICS_DEP}" "${MPGRAFIC_DEP}" "${DTFE_DEP}" "${PLOT_RESULTS_DEP}"; do
            for NEED_LIB in $(echo ${NEED_LIBS}|sed -e 's/;/ /g'); do
                if (dpkg -l | grep "^ii *${NEED_LIB}") ; then
                    echo "...found (but you might wish to update ${NEED_LIB} anyway).";
                else
                    echo "${NEED_LIB} is not yet present in your system."
                    echo "You may be asked in a moment to enter your password"
                    echo "for installing ${NEED_LIB}"
                    echo "with sudo. If you do not trust this script, then ^C and check it again"
                    echo "before proceeding with caution!!!"
                    sleep 1 # wait 1 second
                    # sudo will typically wait for the user to give a password unless s/he ran sudo recently
                    sudo aptitude install ${NEED_LIB}
                fi
            done # for NEED_LIB
        done  # for NEED_LIBS
    fi
    #

    ## Double-check system-level libraries: Abort the script if
    ## system-level package installation failed.
    for NEED_LIBS in "${BASICS_DEP}" "${MPGRAFIC_DEP}" "${DTFE_DEP}" "${PLOT_RESULTS_DEP}"; do
        for NEED_LIB in $(echo ${NEED_LIBS}|sed -e 's/;/ /g'); do
            printf "Checking for ... \n"
            if (dpkg -l | grep "^ii *${NEED_LIB}") ; then
                echo "...found.";
            else
                echo "${NEED_LIB} ... not found. Aborting script."
                printf "You must either install ${NEED_LIB} in your system, or else\n"
                printf "install it in userspace and then modify this script to allow\n"
                printf "usage of that version of ${NEED_LIB}.\n\n"
                exit 1
            fi
        done # for NEED_LIB
    done # for NEED_LIBS


    #    These variables are assumed to be defined in each of the shell
    #    sessions in which you work below.
    #
    #
    # A. MPGRAFIC:
    #
    # *In Debian/Ubuntu:*
    #  Check distribution availability:
    #
    #    https://tracker.debian.org/pkg/mpgrafic
    #    https://launchpad.net/ubuntu/+source/mpgrafic
    #
    # and then
    #
    #  aptitude install mpgrafic
    #  man mpgrafic
    #
    # *In other unixlike systems:*
    #
    # 1 Download from https://bitbucket.org/broukema/mpgrafic .

    clone_codeberg boud mpgrafic ${MPGRAFIC_COMMIT_HASH}

    # 2.1 Adapt Makefile.am and run autoconf if necessary.


    # 3. Configure and compile

    cd mpgrafic
    if [ "x${DO_AUTORECONF}" = "xYES" ]; then
        autoreconf
    fi
    ./configure --prefix=${MYUSR}
    make clean
    make -j ${N_CPUS_DEFAULT}
    ldd src/mpgrafic

    if [ "x${DO_CHECKS}" = "xYES" ]; then
        # to check your installation
        if (make check); then
            printf "mpgrafic passed its check.\n"
        else
            report_system_diagnostics
            printf "mpgrafic failed its check.\n"
            printf "Try 'make check' in the mpgrafic/ directory to investigate.\n"
            exit 1
        fi
    fi

    make install  # to install mpgrafic in ${MYUSR}/bin/

    cd ../


    # B. DTFE-1.1.1.Q:
    #
    # 1. Download from https://bitbucket.org/broukema/dtfe

    clone_codeberg boud dtfe ${DTFE_COMMIT_HASH}

    # 2. Adapt Makefile if necessary.

    # 3. To check your installation:

    cd dtfe
    make clean
    if [ "x${DO_CHECKS}" = "xYES" ]; then
        make tests # && ./test_vgrad_DTFE # TODO: should become "make check"
        if ( ./test_vgrad_DTFE ); then
            printf "DTFE passed its check.\n"
        else
            report_system_diagnostics
            printf "DTFE failed its check.\n"
            printf "Try 'make tests' in the dtfe/ directory to investigate.\n"
            exit 1
        fi
    fi

    #    The test should take about a minute on an 8-core machine.

    make library_static # to make the static library

    cp -pv libDTFE.a ${MYUSR}/lib/

    cd ../

    #exit 0 # debug


    # C. INHOMOG:
    #
    # 1. Download, by default from https://bitbucket.org/broukema/inhomog

    if [ "x${GIT_INHOMOG_DEV}" != "x" ] ; then
        printf "GIT_INHOMOG_DEV option...\n"
        git clone ${GIT_INHOMOG_DEV}
        if ( cd inhomog && (git checkout cosmo-dev && git pull; cd ..) ); then
            cd inhomog && git branch -a && git log --stat |head && cd ..
        else
            printf "Failed to clone inhomog or find the right branch.\n"
            exit 1
        fi
    else
        clone_codeberg boud inhomog ${INHOMOG_COMMIT_HASH}
    fi

    cd inhomog/
    if [ "x${DO_AUTORECONF}" = "xYES" ]; then
        autoreconf
    fi
    ./configure --prefix=${MYUSR} --disable-shared
    ## This is just a test compile - a new configure && compile is done below for
    ## LCDM and EdS separately.
    #CFLAGS="${CFLAGS} -DPRECALC_PRINT_ALL=1 -UINHOM_TURNAROUND_IS_COLLAPSE=1"

    # Bug fix: implement non-parallel printing of I,II,III. This patch will be updated
    # in inhomog in commit 79e1f9f (which as of 2019-12-11 is not yet uploaded to
    # public git repositories).
    patch --verbose -p0 < ${OLD_DIR}/191209_fix_invs.patch

    make clean
    make -j ${N_CPUS_DEFAULT}

    # 2.2 If necessary, use autoreconf and repeat 2.1 .

    # 3. Check the compiled files.

    #   make check # should take about 4 minutes on a 2-core machine
    if [ "x${DO_CHECKS}" = "xYES" ]; then
        if ( make check ); then
            printf "inhomog passed its checks.\n"
        else
            report_system_diagnostics
            printf "inhomog failed its checks.\n"
            printf "Try 'make check' in the inhomog/ directory to investigate.\n"
            exit 1
        fi
    fi

    # 4. To install libinhomog in ${MYUSR}/lib and
    #   inhomog in ${MYUSR}/bin, do

    make install


    # 5. You can now run biscale scale factor VQZA and EdS calculations:

    #   man inhomog
    inhomog --help

    cd ../

    #exit 0 # debug


    # D. RAMSES:

    # 1. Git download from https://bitbucket.org/rteyssie/ramses

    #git clone https://bitbucket.org/rteyssie/ramses

    clone_codeberg boud ramses-use-mpif08 ${RAMSES_COMMIT_HASH}

    cd ramses-use-mpif08/
    if !(git checkout ${RAMSES_COMMIT_BRANCH2}); then
        echo "Could not find branch ${RAMSES_COMMIT_BRANCH2}"
        exit 1
    fi
    WE_HAVE=$(git log --oneline |head -n 1 | awk '{print $1}')
    if [ "x${WE_HAVE}" = "x${RAMSES_COMMIT_HASH2}" ]; then
        echo "The git commit version seems to be correct."
    else
        echo "The git commit version seems to have a problem."
        cd .. # return to root install directory
        exit 1
    fi
    cd .. # return to root install directory

    ln -s ramses-use-mpif08 ramses # symbolic link to use generic ramses name

    cd ramses


    # 2. Follow RAMSES documentation for standard installation and testing.
    #    You will very likely want to modify Makefile a little; you can
    #    later look at the differences between your version and the upstream
    #    version with
    #
    #    git diff Makefile
    #    git diff --word-diff Makefile

    # 3. git checkout a version known to work with ramses-scalav:

    git checkout ${RAMSES_COMMIT_HASH2} # && git log --stat

    cd ../


    #exit 0 # debug


    # E. ramses-scalav:

    # 1. Git download, by default from https://bitbucket.org/broukema/ramses-scalav

    if [ "x${GIT_RAMSES_SCALAV_DEV}" != "x" ] ; then
        printf "GIT_RAMSES_SCALAV_DEV option...\n"
        git clone ${GIT_RAMSES_SCALAV_DEV}
        if ( cd ramses-scalav && (git checkout cosmo-dev && git pull; cd ..) ); then
            cd ramses-scalav && git branch -a && git log --stat |head && cd ..
        else
            printf "Failed to clone ramses-scalav or find the right branch.\n"
            exit 1
        fi
    else
        if [ "x${RUN_NBODY}" = "xYES" ]; then
            ## If the N-body option is set in this script, then choose a different
            ## commit of ramses-scalav
            clone_codeberg boud ramses-scalav ${RAMSES_SCALAV_NBODY_COMMIT_HASH}
        else
            ## Default ramses-scalav version (RZA, not n-body)
            clone_codeberg boud ramses-scalav ${RAMSES_SCALAV_COMMIT_HASH}
        fi
    fi


    # 1.b 2023-08-10 Updates for compiling ramses:
    # - allow less strict argument matching because of MPI
    # - CGAL is now used as source, not as a library
    cd ramses-scalav
    mv -v Makefile.scalav Makefile.scalav.orig
    sed -e "s/^\(FFLAGS.*\)\'/\1 -fallow-argument-mismatch/" \
        -e "s/-lCGAL//" \
        Makefile.scalav.orig > Makefile.scalav
    cd - # return to previous directory



    # 2. Copy the mpgrafic script into your binary directory:

    cd ramses-scalav
    cp -pv mpgrafic_params/mpgrafic_params.sh ${MYUSR}/bin

    # 3. Make a working directory "EdSrefmodel" (EdS reference model) and
    #    try running the mpgrafic_params.sh script:

    cd ../
    mkdir EdSrefmodel
    cd EdSrefmodel
    ## initial check of mpgrafic script
    mpgrafic_params.sh 40 64 # 40 Mpc/h0eff boxsize, N=64^3

    #    You should now have several initial conditions files in EdSrefmodel.
    #
    # 4. Change to your RAMSES directory, symbolically link your
    #    ramses-scalav directory inside of patch/, and symbolically
    #    link the scalav Makefile:
    #
    cd ../ramses/patch/
    ln -s ../../ramses-scalav scalav # give the symbolic name "scalav"
    cd ../bin
    ln -s ../patch/scalav/Makefile.scalav .

    # 5. Adapt Makefile.scalav if necessary. You will probably want to make
    #    changes similar to those you made for the RAMSES Makefile.
    #
    # 6. Compile and install:

    make -f Makefile.scalav -j ${N_CPUS_DEFAULT}
    pwd
    cp -pv ramses3d ${MYUSR}/bin

    # 7. Change back to your EdSrefmodel directory and copy the namelist file
    #    with simulation parameters there:

    cd ../../EdSrefmodel
    # Make a local copy of the simulation parameter file:
    cp -pv ../ramses-scalav/namelist/cosmo.nml.EdSrefmodel.${N_CROOT}cubed .

    ## For the N-body option, override the default simulation parameter file:
    ## choose output scale factors to match those of Fig 1c of v1;
    ## increase the particle and grid numbers by about a factor of 10;
    ## disable the RZA option; enable the N-body option.
    ## This assumes typical values for numbers that are multiples of 10.
    if [ "x${RUN_NBODY}" = "xYES" ]; then
        cat ../ramses-scalav/namelist/cosmo.nml.EdSrefmodel.${N_CROOT}cubed | \
            sed -e "s/noutput=.*\'/noutput=5/" | \
            sed -e "s/aout=.*\'/aout=0.16,0.38,0.54,0.69,0.83/" | \
            sed -e 's/nparttot *= *\([1-9][0-9]*\)0/nparttot=\100/' | \
            sed -e 's/ngridtot *= *\([1-9][0-9]*\)0/ngridtot=\100/' | \
            sed -e 's/RZA_no_nbody=\.true\./RZA_no_nbody=\.false\./' | \
            sed -e 's/Raychaudhuri_nbody=\.false\./Raychaudhuri_nbody=\.true\./' > \
                cosmo.nml.EdSrefmodel.${N_CROOT}cubed
    fi


    # 8. Output some diagnostics in case things go wrong after this
    check_binary_path mpgrafic ${MYUSR}/bin/mpgrafic
    check_binary_path mpgrafic_params.sh ${MYUSR}/bin/mpgrafic_params.sh
    check_binary_path ramses3d ${MYUSR}/bin/ramses3d

    report_system_diagnostics


    # F. Do calculations and plot them:

    if [ $(basename $(pwd)) != "EdSrefmodel" ]; then
        printf "You seem to be in the directory %s but you should be in EdSrefmodel. Something went wrong.\n" $(basename $(pwd))
        exit 1
    fi


    for FLRW in ${FLRW_LIST}; do

        # 0. recompile inhomog and ramses
        printf "Starting FLRW=${FLRW} loop:\n"
        cd ../inhomog
        #RZA2_54="-DINHOM_OMEGA_R_D_RZA2_54"
        RZA2_54=
        if [ "x${FLRW}" = "xLCDM" ]; then
            ./configure --prefix=${MYUSR} --disable-shared \
                        CFLAGS="${CFLAGS} ${RZA2_54} -DPRECALC_PRINT_ALL -DINHOM_TURNAROUND_IS_COLLAPSE -DINHOM_LCDM"
        else
            ./configure --prefix=${MYUSR} --disable-shared \
                        CFLAGS="${CFLAGS} ${RZA2_54} -DPRECALC_PRINT_ALL -DINHOM_TURNAROUND_IS_COLLAPSE -DINHOM_EDS"
        fi
        make clean
        make -j ${N_CPUS_DEFAULT}
        make install

        cd ../ramses/bin
        make -f Makefile.scalav
        pwd
        cp -pv ramses3d ${MYUSR}/bin

        cd ../../EdSrefmodel # TODO: LCDMrefmodel for LCDM ?
        gzip scalav.${FLRW}*Lbox*L_D*log || true # compress old log files; alternative: delete them

        ## LBOX in Mpc/hzeroeff
        for LBOX_FACT16 in ${LBOX_FACT16_LIST}; do
            # 1. run mpgrafic to generate initial conditions
            # Usage: mpgrafics_params.sh BOXSIZE_H0EFF_MPC MPGRAFIC_NP [[[LCDM|EdS] N_CORES] SEED]"
            case ${N_CROOT} in
                256|128|64)
                    LBOX=${LBOX_FACT16}
                    ;;
                32)
                    LBOX=$(echo ${LBOX_FACT16} |awk '{printf("%03d",$1/2)}')
                    ;;
                *)
                    printf "No other N_CROOT options. Please check the script and parameter files.\n"
                    exit 1
                    ;;
            esac
            echo "After LBOX update, ${LBOX}=LBOX."

            mpgrafic_params.sh ${LBOX} ${N_CROOT} ${FLRW} ${N_CPUS_MPGRAFIC}

            # 2. Run ramses-scalav, e.g. with 4 cores for MPI, and unlimited cores
            #    for openmp (DTFE), saving the results to a file, e.g.
            F=cosmo.nml.EdSrefmodel.${N_CROOT}cubed

            ## Prepare a main label for output files, based on the box size
            ## and domain size, as defined by this particular namelist file:
            N_CROOT_CHECK=$(grep "^levelmin.*=" ${F}|tail -n1 |awk -F '=' '{print 2^$2}')
            N_SCALAV_SCALE=$(grep "^scal_av_n_gridsize.*=" ${F}|tail -n1 |awk -F '=' '{print $2}')
            L_DOM=$(echo ${N_SCALAV_SCALE}| \
                           awk -v lbox=${LBOX} '{printf("%05.1f",lbox/$1)}')
            export RAMSES_SCALAV_LOG_FILE="scalav.${FLRW}.Lbox${LBOX}.L_D${L_DOM}.$(date +%Y%m%d-%H%M%S).log"

            mpirun -n ${N_CPUS_RAMSES} ${MYUSR}/bin/ramses3d cosmo.nml.EdSrefmodel.${N_CROOT}cubed > ${RAMSES_SCALAV_LOG_FILE}

            # plot similar to Fig 7 of Roukema (2018):
            #(grep ^a_D_global ${RAMSES_SCALAV_LOG_FILE} |awk '{print $2,$3}' ; \
            # printf "\n"; \
            # grep ^a_D_global ${RAMSES_SCALAV_LOG_FILE} |awk '{print $2,$5}' ) |\
            #    graph -TX -X "t (Gyr)" -Y "a(t) [EdS or eff]"

            # plot(s) for RO19
            export RAMSES_SCALAV_DAT_FILE="$(basename ${RAMSES_SCALAV_LOG_FILE} ".log").dat"
            export RAMSES_SCALAV_INV_FILE="$(basename ${RAMSES_SCALAV_LOG_FILE} ".log").inv"
            export RAMSES_SCALAV_AEFF_FILE="$(basename ${RAMSES_SCALAV_LOG_FILE} ".log").aeff"
            if [ "x${RUN_NBODY}" = "xYES" ]; then
                export RAMSES_SCALAV_NBODY_FILE="$(basename ${RAMSES_SCALAV_LOG_FILE} ".log").nbody"
            fi
            printf "Plottable file with t, H_D, OmR_D: %s\n" ${RAMSES_SCALAV_DAT_FILE}

            grep "^ tB2" ${RAMSES_SCALAV_LOG_FILE} | \
                awk '{print $12,$14,$15,$17,$18,$20,$21,$22,$23}' > ${RAMSES_SCALAV_DAT_FILE}
            if [ "x${RUN_NBODY}" = "xYES" ]; then
                grep "^OmDlist::" ${RAMSES_SCALAV_LOG_FILE} | \
                    awk '{print $2,$3,$4,$5,$6}' > ${RAMSES_SCALAV_NBODY_FILE}
            fi

            grep "^I,II,III" ${RAMSES_SCALAV_LOG_FILE} | \
                awk '{print $3,$4,$5}' > ${RAMSES_SCALAV_INV_FILE}

            grep "^a_D_global:" ${RAMSES_SCALAV_LOG_FILE} | \
                awk '{print $2,$5}' > ${RAMSES_SCALAV_AEFF_FILE}

        done; ## for LBOX in 640 160 040; do
    done
    cd ../ ## return to ${FULL_INSTALL_DIR}

    # The plotting function can be called from outside of the
    # 'if [ "x${REALLY}" = "xYES" ]'
    # loop in case you need some extra steps installing octave or tex packages.
    do_the_plots

fi #  if [ "x${REALLY}" = "xYES" ] ; then

## go back to directory from where the script was launched
cd ${OLD_DIR}
printf "You should now have the figures in the file ${FULL_INSTALL_DIR}/show_figures.pdf .\n"

## restore original LDFLAGS in case the value is exported back out
LDFLAGS=${OLD_LDFLAGS}
